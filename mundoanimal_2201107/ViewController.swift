//
//  ViewController.swift
//  mundoanimal_2201107
//
//  Created by COTEMIG on 17/11/22.
//

import UIKit
import Kingfisher
import Alamofire

struct MundoAnimal: Decodable {
    let name: String
    let latin_name: String
    let image_link: String
}

class ViewController: UIViewController {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var latim: UILabel!
    @IBOutlet weak var name: UINavigationItem!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getApiMundoAnimal()
    }
    
    func getApiMundoAnimal() {
            AF.request("https://zoo-animal-api.herokuapp.com/animals/rand").responseDecodable(of: MundoAnimal.self){
                response in
                if let animal = response.value {
                    self.image.kf.setImage(with: URL(string: animal.image_link))
                    self.latim.text = animal.latin_name
                    self.name.title = animal.name
                }
            }
        }
}
