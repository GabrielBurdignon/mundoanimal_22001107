//
//  MinhaCelula.swift
//  mundoanimal_2201107
//
//  Created by COTEMIG on 17/11/22.
//

import UIKit

class MinhaCelula: UITableViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var image_link: UIImageView!
    @IBOutlet weak var latim_name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
